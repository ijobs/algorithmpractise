#!/bin/bash

source ./config.sh

# create test in and out dir
makeTestDir()
{

}

if [ "$#" -ne 1 ]
then
	echo "Usage: new_contest.sh ContestName"
	exit
fi

CONTEST_PATH=${CONTEST_ROOT}"/"$1
if [ ! -e $CONTEST_PATH ]
then
	mkdir $CONTEST_PATH
fi

TPL_PATH=${TOOL_ROOT}"/tpl/cpp_tpl.cpp"

i=1
while [ $i -le 5 ]
do
	cp ${TPL_PATH} "${CONTEST_PATH}/${i}.cpp"
	
	let "i+=1"
done
